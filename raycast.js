function calcCollsLineWalls() {
  allRays.forEach((ray) => {
    var shortestDistance = 1000000;

    walls.forEach((pared) => {
      let puntos = lineaColisiona(ray.line, pared);

      if (puntos.length !== 0) {
        let estaMasCerca = calcSHortPoint(puntos);

        if (estaMasCerca < shortestDistance) {
          shortestDistance = estaMasCerca;
          ray.shortDist = estaMasCerca;
        }
      }
    });
  });
}
