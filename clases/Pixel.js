class Pixel {
  constructor(x, y, width, height, color) {
    this.color = color;
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
  }
}
