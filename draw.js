function redraw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "#333333";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  reDraw2();

  // Lineas de vision
  /*ctx.strokeStyle = "#000000";
  playerVisionLines.forEach((line) => {
    ctx.beginPath();
    drawLine(line);
    ctx.stroke();
  });
*/

  ctx.strokeStyle = "#dddddd";
  ctx.fillStyle = "#dddddd";
  ctx.beginPath();

  ctx.moveTo(player.x, player.y);
  allRays.forEach((ray) => {
    drawAllShorterCircles(ray);
  });
  ctx.fill();
  ctx.stroke();

  walls.forEach((wall) => {
    drawSquare(wall);
  });

  todosPuntos = [];
  drawPLayer();
}

function drawAllShorterCircles(ray) {
  ctx.arc(
    CalcSecondPointAngleBaseCos(ray.angle, ray.shortDist),
    CalcSecondPointAngleBaseSin(ray.angle, ray.shortDist),
    0,
    2 * Math.PI,
    false
  );
}

function drawLine(line) {
  ctx.moveTo(line.x1, line.y1);
  ctx.lineTo(line.x2, line.y2);
}

function drawSquare(square) {
  ctx.fillStyle = square.color;
  ctx.fillRect(square.x, square.y, square.width, square.height);
}

function drawPLayer() {
  ctx.beginPath();
  ctx.fillStyle = "#00ba00";
  ctx.arc(player.x, player.y, player.size, 2 * Math.PI, false);
  ctx.fill();

  //////////////////////////////////////////////////////

  ctx.beginPath();
  ctx.fillStyle = "#000000";
  ctx.strokeStyle = "#ffffff";

  ctx.arc(
    CalcSecondPointAngleBaseCos(player.rd, player.size * 0.8),
    CalcSecondPointAngleBaseSin(player.rd, player.size * 0.9),
    player.size / 2.8,
    2 * Math.PI,
    false
  );
  ctx.stroke();
  ctx.fill();

  ///////////////////////////////////////////////////

  ctx.beginPath();
  ctx.fillStyle = "#000000";
  ctx.strokeStyle = "#ffffff";

  ctx.arc(
    CalcSecondPointAngleBaseCos(player.ld, player.size * 0.8),
    CalcSecondPointAngleBaseSin(player.ld, player.size * 0.9),
    player.size / 2.8,
    2 * Math.PI,
    false
  );
  ctx.stroke();
  ctx.fill();
  ctx.strokeStyle = "#000000";
}

setInterval(() => {
  reCalc();
  redraw();
}, 1);
