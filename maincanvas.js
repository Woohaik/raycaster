var canvas2 = document.getElementById("main");
var ctx2 = canvas2.getContext("2d");

function reDraw2() {
  ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
  ctx2.fillStyle = "#82dcfe";
  ctx2.fillRect(0, 0, canvas2.width, canvas2.height / 2);

  var grd = ctx.createRadialGradient(canvas2.width / 2, canvas2.height, 10, canvas2.width / 2, canvas2.height, 300);
  grd.addColorStop(0, "#a3ffa7");
  grd.addColorStop(1, "#07ad0f");
  ctx2.fillStyle = grd;

  ctx2.fillRect(0, canvas2.height / 2, canvas2.width, canvas2.height);

  squaresRaycasteds.forEach((pixel) => {
    ctx2.fillStyle = rgbToHex(
      parseInt(pixel.color, 10),
      parseInt(pixel.color, 10),
      parseInt(pixel.color, 10)
    );
    ctx2.fillRect(pixel.x, pixel.y, pixel.width, pixel.height);
  });
}

function componentToHex(c) {
  let hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}
function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}
