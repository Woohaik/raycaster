var lineSize = calcHiponeous(canvas.width, canvas.height);

function calcHiponeous(w, h) {
    return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2));
  }
  

let numRays = 250;
let walls = [];
let playerVisionLines = [];
var todosPuntos = [];
let allRays = [];


let squaresRaycasteds = [];

let pruebaShortDistance = 0;

playerVisionLines.push(
  new Linea(
    player.x,
    player.y,
    CalcSecondPointAngleBaseCos(player.rd),
    CalcSecondPointAngleBaseSin(player.rd)
  )
);
playerVisionLines.push(
  new Linea(
    player.x,
    player.y,
    CalcSecondPointAngleBaseCos(player.ld),
    CalcSecondPointAngleBaseSin(player.ld)
  )
);

for (let x = 0; x < gameMap.length; x++) {
  for (let y = 0; y < gameMap[x].length; y++) {
    if (gameMap[x][y] !== 0)
      walls.push(new Wall(y * wallWidth, x * wallHeight, "#7783e0"));
  }
}

function toRadian(degrees) {
  return degrees * (pi / 180);
}

function CalcSecondPointAngleBaseCos(angle, distance = lineSize) {
  return player.x - distance * Math.cos(toRadian(angle));
}

function CalcSecondPointAngleBaseSin(angle, distance = lineSize) {
  return player.y - distance * Math.sin(toRadian(angle));
}
