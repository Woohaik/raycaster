var map = {}; // You could also use an array
document.addEventListener("keydown", logKey);
document.addEventListener("keyup", (e) => {
  delete map[e.key];
  movement();
});
function logKey(e) {
  map[e.key] = e.key;
  movement();
}

function movement() {
  if (map.w) {
    if (!hayColision(0, -(player.size ))) player.y -= player.speed;
  }
  if (map.s) {
    if (!hayColision(0, player.size)) player.y += player.speed;
  }
  if (map.a) {
    if (!hayColision(-player.size, 0)) player.x -= player.speed;
  }
  if (map.d) {
    if (!hayColision(player.size, 0)) player.x += player.speed;
  }

  if (map.ArrowLeft) {
    player.direction -= 5;
  }

  if (map.ArrowRight) {
    player.direction += 5;
  }
}



function hayColision(numComprbarX, numComprbarY) {
  var newXPoint = player.x + numComprbarX;
  var newYPoint = player.y + numComprbarY;
  var toReturn = false;
  walls.forEach((element) => {
    if (newXPoint >= element.x && newXPoint <= element.x + wallWidth) {
      if (newYPoint >= element.y && newYPoint <= element.y + wallHeight) {
        toReturn = true;
      }
    }
  });

  return toReturn;
}
