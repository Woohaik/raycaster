function lineaColisiona(line, rect) {
  var p1 = { x: line.x1, y: line.y1 };
  var p2 = { x: line.x2, y: line.y2 };
  let puntosInterseccion = [];

  // Linea Superior de cuadro
  var q = { x: rect.x, y: rect.y };
  var q2 = { x: rect.x + rect.width, y: rect.y };

  let l1 = lineSegmentsIntercept(p1, p2, q, q2);
  if (l1 !== false) puntosInterseccion.push(l1);

  // Linea derecha de cuadro
  var q = q2;
  var q2 = { x: rect.x + rect.width, y: rect.y + rect.height };

  let l2 = lineSegmentsIntercept(p1, p2, q, q2);
  if (l2 !== false) puntosInterseccion.push(l2);

  // Linea de abajo de cuadro
  var q = q2;
  var q2 = { x: rect.x, y: rect.y + rect.height };

  let l3 = lineSegmentsIntercept(p1, p2, q, q2);
  if (l3 !== false) puntosInterseccion.push(l3);

  // Linea Izquierda de cuadro.
  var q = q2;
  var q2 = { x: rect.x, y: rect.y };

  let l4 = lineSegmentsIntercept(p1, p2, q, q2);
  if (l4 !== false) puntosInterseccion.push(l4);

  if (puntosInterseccion.length !== 0) {
    return puntosInterseccion;
  } else {
    let arr = [];
    return arr;
  }
}

function calcRayAngle() {
  return player.vision / numRays;
}

function reCalcAllLInes() {
  for (let index = 0; index < numRays; index++) {
    allRays[index] = {
      line: new Linea(
        player.x,
        player.y,
        CalcSecondPointAngleBaseCos(
          player.direction - player.vision / 2 + index * calcRayAngle()
        ),
        CalcSecondPointAngleBaseSin(
          player.direction - player.vision / 2 + index * calcRayAngle()
        )
      ),
      angle: player.direction - player.vision / 2 + index * calcRayAngle(),
    };
  }
}

function reCalc() {
  calcAngle();
  calcPLayerLines();
  reCalcAllLInes();
  calcCollsLineWalls();
  calcSquaresRaycasteds();
}

function calcSquaresRaycastedsHeights(rayLenght) {
  let Maxcolor = 210;

  let maxSize = lineSize - ((lineSize/ gameMap[0].length) )
  

  let s = rayLenght / maxSize;
  let porcent = (maxSize - maxSize * s) / maxSize;

  return {
    color: Maxcolor * porcent,
    size: (canvas2.height * porcent),
  };
}

function calcY(size) {
  return (canvas2.height - size) / 2;
}

function calcSquaresRaycasteds() {
  squaresRaycasteds = [];
  allRays.forEach((ray) => {
    let squareVariables = calcSquaresRaycastedsHeights(ray.shortDist);

    let PixelX = (canvas2.width / allRays.length) * allRays.indexOf(ray);
    let PixelY = calcY(squareVariables.size);
    let PixelW = canvas2.width / allRays.length;

    squaresRaycasteds.push(
      new Pixel(
        PixelX,
        PixelY,
        PixelW,
        squareVariables.size,
        squareVariables.color
      )
    );
  });
}

function calcDistance(point) {
  let primerParentesis = Math.pow(player.x - point.x, 2);
  let SegundoParentesis = Math.pow(player.y - point.y, 2);
  let resultado = Math.sqrt(primerParentesis + SegundoParentesis);
  return resultado;
}

function comparar(a, b) {
  return a - b;
}

function calcSHortPoint(arrayPoints) {
  let arrayOfDistance = [];

  arrayPoints.forEach((point) => {
    arrayOfDistance.push(calcDistance(point));
  });

  arrayOfDistance.sort(comparar);

  return arrayOfDistance[0];
}

function calcPLayerLines() {
  playerVisionLines[0].x1 = player.x;
  playerVisionLines[0].y1 = player.y;
  (playerVisionLines[0].x2 = CalcSecondPointAngleBaseCos(player.rd)),
    (playerVisionLines[0].y2 = CalcSecondPointAngleBaseSin(player.rd));

  playerVisionLines[1].x1 = player.x;
  playerVisionLines[1].y1 = player.y;
  (playerVisionLines[1].x2 = CalcSecondPointAngleBaseCos(player.ld)),
    (playerVisionLines[1].y2 = CalcSecondPointAngleBaseSin(player.ld));
}

function calcAngle() {
  player.ld = player.direction - player.vision / 2;
  player.rd = player.direction + player.vision / 2;
}

var lineSegmentsIntercept = (function () {
  var v1, v2, v3, cross, u1, u2;

  v1 = { x: null, y: null };
  v2 = { x: null, y: null };
  v3 = { x: null, y: null };

  function lineSegmentsIntercept(p0, p1, p2, p3) {
    v1.x = p1.x - p0.x;
    v1.y = p1.y - p0.y;
    v2.x = p3.x - p2.x;
    v2.y = p3.y - p2.y;
    if ((cross = v1.x * v2.y - v1.y * v2.x) === 0) {
      return false;
    }
    v3 = { x: p0.x - p2.x, y: p0.y - p2.y };
    u2 = (v1.x * v3.y - v1.y * v3.x) / cross;

    if (u2 >= 0 && u2 <= 1) {
      u1 = (v2.x * v3.y - v2.y * v3.x) / cross;

      if (u1 >= 0 && u1 <= 1) {
        return {
          x: p0.x + v1.x * u1,
          y: p0.y + v1.y * u1,
        };
      }
    }
    return false;
  }
  return lineSegmentsIntercept;
})();
